<?php

namespace App\Http\Controllers;

use App\BirthColony;
use App\CivilStatus;
use App\Company;
use App\Employee;
use App\FederalEntity;
use App\Imports\EmployeesImport;
use App\Municipality;
use App\NationalityMode;
use App\Sex;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Maatwebsite\Excel\Facades\Excel;

class EmployeeController extends Controller
{
    public function importExcel(Request $request)
    {
        $file = $request->file('file');
        // dd($file);
        $list = Excel::toCollection(new EmployeesImport, $file);
        // verificar hojas de excel
        $van = 0;
        // reportar errores
        $collection = collect([]);
        $campKey    = collect([]);
        foreach ($list as $registers) {
            foreach ($registers as $people) {

                // verificar si existen mas de un valor nulo
                // comprobar campos claves dstintos a null
                $resp = $this->valiNull($people);
                if ($resp == 1) {

                    // validar clave estado civil
                    $respon = $this->valCivilStatu($people);
                    if ($respon == 0) {
                        $people['error'] = 'validar clave estado civil';
                        $campKey->push($people);
                        continue;
                    }
                    $people['estado_civil'] = $respon;

                    // validar clave nacionalidad
                    $respon = $this->valNacionality($people);
                    if ($respon == 0) {
                        $people['error'] = 'validar clave nacionalidad';

                        $campKey->push($people);
                        continue;
                    }
                    $people['modo_de_nacionalidad'] = $respon;

                    // validar clave entidad federativa
                    $respon = $this->valFederalEntity($people);
                    if ($respon == 0) {
                        $people['error'] = 'validar clave entidad federativa';
                        $campKey->push($people);
                        continue;
                    }
                    $people['entidad_de_nacimiento'] = $respon;

                    // validar clave sexo
                    $respon = $this->valSex($people);
                    if ($respon == 0) {
                        $people['error'] = 'validar clave sexo';
                        $campKey->push($people);
                        continue;
                    }
                    $people['sexo'] = $respon;

                    // validar clave empresa
                    $respon = $this->valCompany($people);
                    if ($respon == 0) {
                        $people['error'] = 'validar clave empresa';
                        $campKey->push($people);
                        continue;
                    }
                    $people['empresa'] = $respon;

                    // validar clave colonia de nacimiento
                    $respon = $this->valBirthColony($people);
                    if ($respon == 0) {
                        $people['colonia_de_nacimiento'] = null;
                    } else {
                        $people['colonia_de_nacimiento'] = $respon;
                    }

                    // validar clave municipio
                    $respon = $this->valMunicipality($people);
                    if ($respon == 0) {
                        $people['municipio_de_nacimiento'] = null;
                    } else {
                        $people['municipio_de_nacimiento'] = $respon;
                    }
                    $this->save($people);
                    $van++;
                } else {
                    $people['error'] = 'validar claves null';
                    // registros con muchos campos vacios
                    $campKey->push($people);

                }

            }
        }
        // dd($campKey);
        return view('completo', compact('campKey', 'van'))->with('message', 'Importanción de usuarios completada');
        // return back()->compact('campKey', 'van'))->with('message', 'Importanción de usuarios completada');
    }

    public function valiNull($row)
    {
        if ($row['nombres'] != null &&
            $row['apellido_paterno'] != null &&
            $row['apellido_materno'] != null &&
            $row['fecha_de_nacimiento'] != null &&
            $row['estado_civil'] != null &&
            $row['rfc'] != null &&
            $row['clave_del_ife'] != null &&
            $row['curp'] != null &&
            $row['afiliacion_a_imss'] != null &&
            $row['clave_de_elector'] != null &&
            $row['modo_de_nacionalidad'] != null &&
            $row['entidad_de_nacimiento'] != null &&
            $row['sexo'] != null &&
            $row['empresa'] != null) {

            return 1;
        } else {
            return 0;
        }

    }

    public function valCivilStatu($row)
    {
        $val = trim($row['estado_civil'], " ");
        $val = CivilStatus::where('nombre', $val)->first();

        if ($val != null) {
            return $val->oid;
        }
        return 0;
    }

    public function valNacionality($row)
    {
        $val = trim($row['modo_de_nacionalidad'], " ");
        $val = NationalityMode::where('nombre', $val)->first();
        if ($val != null) {
            return $val->oid;
        }
        return 0;
    }

    public function valFederalEntity($row)
    {
        $val = trim($row['entidad_de_nacimiento'], " ");
        $val = FederalEntity::where('nombre', $val)->first();
        if ($val != null) {
            return $val->oid;
        }
        return 0;
    }

    public function valMunicipality($row)
    {
        $val = trim($row['municipio_de_nacimiento'], " ");
        $val = Municipality::where('nombre', $val)->first();
        if ($val != null) {
            return $val->oid;
        }
        return 0;
    }

    public function valBirthColony($row)
    {
        $val = trim($row['colonia_de_nacimiento'], " ");
        $val = BirthColony::where('nombre', $val)->first();
        if ($val != null) {
            return $val->oid;
        }
        return 0;
    }

    public function valSex($row)
    {
        $val = trim($row['sexo'], " ");
        $val = Sex::where('nombre', $val)->first();
        if ($val != null) {
            return $val->oid;
        }
        return 0;
    }
    public function valCompany($row)
    {
        $val = Company::all();
        // dd($resultado, $row['empresa']);
        foreach ($val as $key) {
            $resultado = substr($key->rfc, 0, 3);
            $value     = trim($row['empresa'], " ");
            if ($resultado == $value) {
                return $key->oid;
            }
        }
        return 0;
    }
    public function save($row)
    {
        // dd($collection);
        // guardar en la bd
        $rfc = trim($row['rfc'], " ");
        return Employee::create([
            'nombre'                => $row['nombres'],
            'apellidoPaterno'       => $row['apellido_paterno'],
            'apellidoMaterno'       => $row['apellido_materno'],

            'fechaNacimiento'       => Carbon::instance(\PhpOffice\PhpSpreadsheet\Shared\Date::excelToDateTimeObject($row['fecha_de_nacimiento'])),
            'sexoId'                => $row['sexo'],
            'RFC'                   => $rfc,
            'claveIFE'              => (string) $row['clave_del_ife'],
            'claveElector'          => $row['clave_de_elector'],
            'telefono'              => (string) $row['telefono'],
            'CURP'                  => $row['curp'],
            'estadoCivilId'         => $row['estado_civil'],
            'IMSS'                  => (string) $row['afiliacion_a_imss'],
            'fechaContrato'         => Carbon::instance(\PhpOffice\PhpSpreadsheet\Shared\Date::excelToDateTimeObject($row['fecha_de_contrato'])),
            'modoNacionalidadId'    => $row['modo_de_nacionalidad'],
            'entidadNacimientoId'   => $row['entidad_de_nacimiento'],
            'municipioNacimientoId' => $row['municipio_de_nacimiento'],
            'coloniaNacimientoId'   => $row['colonia_de_nacimiento'],
            'nombreCompleto'        => $row['nombres'] . " " . $row['apellido_paterno'] . " " . $row['apellido_materno'],
            'empresaId'             => $row['empresa'],

        ]);

    }

}
