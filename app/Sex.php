<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Sex extends Model
{
    protected $table = 'sexo';

    protected $primaryKey = 'oid';

    protected $fillable = [
        'nombre',
    ];
}
