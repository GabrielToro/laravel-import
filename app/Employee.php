<?php

namespace App;

use App\CivilStatus;
use Illuminate\Database\Eloquent\Model;

class Employee extends Model
{
    protected $table      = 'empleado';
    protected $primaryKey = 'oid';

    protected $fillable = [
        'claveElector',
        'claveIFE',
        'nombreCompleto',
        'empresaId',
        'nombre',
        'apellidoPaterno',
        'apellidoMaterno',
        'sexoId',
        'RFC',
        'claveIFE',
        'claveElector',
        'telefono',
        'CURP',
        'estadoCivilId',
        'IMSS',
        'fechaContrato',
        'modoNacionalidadId',
        'fechaNacimiento',
        'entidadNacimientoId',
        'municipioNacimientoId',
        'coloniaNacimientoId',
    ];
    public $timestamps = false;

    public function CivilStatus()
    {
        return $this->belongsTo(CivilStatus::class, 'estadoCivilId');
    }

}
