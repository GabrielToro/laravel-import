<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class BirthColony extends Model
{
    protected $table = 'colonia';

    protected $primaryKey = 'oid';

    protected $fillable = [
        'descripcion',
        'municipioId',
        'nombre',
        'oid',
    ];
}
