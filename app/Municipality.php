<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Municipality extends Model
{
    protected $table = 'municipio';

    protected $primaryKey = 'oid';

    protected $fillable = [
        'descripcion',
        'entidadFederativaId',
        'nombre',
        'oid',
    ];
}
