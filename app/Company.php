<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Company extends Model
{

    protected $table = 'empresa';

    protected $primaryKey = 'oid';

    protected $fillable = [
        'nombre',
        'activo',
        'dependencia',
        'descripcion',
        'direccionId',
        'email',
        'institucion',
        'nombreDirectorGeneral',
        'oid',
        'representanteLegal',
        'rfc',
        'sectorId',
        'telefono',
    ];

}
