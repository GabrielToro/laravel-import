<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class FederalEntity extends Model
{
    protected $table = 'entidadfederativa';

    protected $primaryKey = 'oid';

    protected $fillable = [
        'nombre', 'descripcion',
    ];
}
