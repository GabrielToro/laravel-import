<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CivilStatus extends Model
{
    protected $table = 'estado_civil';

    protected $primaryKey = 'oid';

    protected $fillable = [
        'nombre',
    ];
}
