<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class NationalityMode extends Model
{
    protected $table = 'modonacionalidad';

    protected $primaryKey = 'oid';

    protected $fillable = [
        'nombre',
    ];
}
