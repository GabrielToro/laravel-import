<?php

namespace App\Imports;

use Maatwebsite\Excel\Concerns\ToModel;
use Maatwebsite\Excel\Concerns\WithHeadingRow;

class EmployeesImport implements ToModel, WithHeadingRow
{

    public function model(array $row)
    {

        return 1;
    }

    public function headingRow(): int
    {
        return 2;
    }

}
