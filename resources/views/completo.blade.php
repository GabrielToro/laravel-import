@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
  

                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif

          
            

            <p>{{ Session::get('message') }}</p>
            <p>Felicidades Se guardaron {{ $van }} empleados </p>
            <p>Ocurrieron :{{ $campKey->count() }} Errores en la inserccion </p>
            <table class="table">

                  <thead>
                    <tr>
                      <th scope="col">#</th>
                      <th scope="col">nombre </th>
                      <th scope="col">apellido</th>
                      <th scope="col">error</th>
                    </tr>
                  </thead>
                  <tbody>
                    @php
                        $cont=1;
                    @endphp
                    @foreach ($campKey as $key)
                           <tr>
                              <th scope="row">{{ $cont++ }}</th>
                              <td>{{ $key['nombres'] }}</td>
                              <td>{{ $key['apellido_paterno']}}</td>
                              <td>{{ $key['error'] }}</td>
                            </tr>
                    @endforeach
                    
                    
                  </tbody>
            </table>
          
          
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
